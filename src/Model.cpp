#include "Model.hpp"
//SOIL
#include "../inc/soil.h"

//for test
#include <iostream>


Model::Model(std::string uname)
{ //open and load "models/{$uname}.obj
    name = uname;
    std::string strings[] = {"models/", uname,".obj"};
    loadOBJ( mStrCat(strings).c_str(), vertices, uvs, normals);
    //gen buffers
    glGenBuffers(1, &vertexbuffer);
    glGenBuffers(1, &uvbuffer);
    glGenBuffers(1, &normalbuffer);
    //defaults
    normal = glm::vec3(0,0,-1);
    pivot = glm::vec3(0,0,0);
    transform = glm::mat4x4(1.0);
}
//getters
std::string Model::getName(){ return name; }
const std::vector<glm::vec2> Model::getUvs(){ return uvs; }
const std::vector<glm::vec3> Model::getNormals(){ return normals; }
const std::vector<glm::vec3> Model::getVertices(){ return vertices; }
const glm::mat4x4 Model::getTransform() { return transform; }
const glm::vec3 Model::getPivot(){ return pivot; }

int Model::getVerticesNum() { return vertices.size(); }

void Model::moveForward( float m )
{
    glm::vec3 moving = m*normal;
    float x,y,z;

    move((float)moving.x, (float)moving.y, (float)moving.z);
}

void Model::loadTexture(std::string textName)
{
    unsigned char* textureImage;
    int textureHeight;
    int textureWidth;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    //texture settings
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    //get filename
    std::string strs[] = {"textures/", textName,".png"};

    //error
    if(!fileexists(mStrCat(strs).c_str())){
        printf("Error: %s file is not exist!\n", mStrCat(strs).c_str());
        return;
    }
    printf("%s for texture load.\n", mStrCat(strs).c_str());
    //get image
    textureImage = SOIL_load_image(mStrCat(strs).c_str(), &textureWidth, &textureHeight, 0, SOIL_LOAD_RGB);
    if(!textureImage)
        printf("Texture loading failed! (%s)\n", mStrCat(strs).c_str());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textureWidth, textureHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, textureImage);
    SOIL_free_image_data(textureImage);
}

//transforms
void Model::mirror()
{
    // vec1 axis
    // vec2 obj normal
    glm::vec3 planeNormal = glm::cross(glm::vec3(0,1,0),normal);
}
void Model::scale(float s)
{
    transform *= s;
    normal *= s;
    pivot *= s;

    for(int i=0; i < Model::getVerticesNum(); i++)
    {
        vertices[i] *= s;
    }

}
void Model::rotate(float angle, glm::vec3 n)
{
    //calculate moving vector
    glm::vec3 mov = pivot - glm::rotate(pivot,angle,n);

    normal = glm::rotate(normal,angle,n);
    transform = glm::rotate(transform,angle,-n);
    transform = glm::translate(transform, -mov);
    //cacl rotatated pivot

    for(int i=0; i < Model::getVerticesNum(); i++)
    {
        vertices[i] = glm::rotate(vertices[i], angle, n);
        vertices[i] += mov;
    }

}
void Model::rotateY(float angle)
{
    normal = glm::rotateY(normal,angle);
    pivot = glm::rotateY(pivot,angle);
    transform = glm::rotate(transform,angle,glm::vec3(0,-1,0));
    for(int i=0; i < Model::getVerticesNum(); i++)
    {
        vertices[i] = glm::rotateY(vertices[i], angle);
    }
}
void Model::move(glm::vec3 v)
{
    move(v.x,v.y,v.z);
}
void Model::move(float x, float y, float z)
{
    transform = glm::translate(transform, -glm::vec3(x, y, z));
    pivot += glm::vec3(x, y, z);
    for(int i=0; i < Model::getVerticesNum(); i++)
    {
        vertices[i].x += x;
        vertices[i].y += y;
        vertices[i].z += z;
    }
}
int Model::getVertSize()
{
    return vertices.size() * sizeof( glm::vec3);
}
void Model::loadVBO()
{
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_DYNAMIC_DRAW);
}

void Model::render()
{
    //glActiveTexture(0);
    //
    glActiveTexture(texture);
	glUniform1i(uniform_mytexture, texture);
	glBindTexture(GL_TEXTURE_2D, texture);


    // 1rst attribute buffer : vertices
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(
        0,                  // attribute
        3,                  // size
        GL_FLOAT,           // type
        GL_FALSE,           // normalized?
        0,                  // stride
        (void*)0            // array buffer offset
    );

    // 2nd attribute buffer : UVs
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
    glVertexAttribPointer(
        1,                                // attribute
        2,                                // size
        GL_FLOAT,                         // type
        GL_FALSE,                         // normalized?
        0,                                // stride
        (void*)0                          // array buffer offset
    );

    // 3rd attribute buffer : normals
    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
    glVertexAttribPointer(
        2,                                // attribute
        3,                                // size
        GL_FLOAT,                         // type
        GL_FALSE,                         // normalized?
        0,                                // stride
        (void*)0                          // array buffer offset
    );


    // Draw the triangles
    glDrawArrays(GL_TRIANGLES, 0, getVerticesNum() );
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
}
void Model::setPivot(glm::vec3 p) {  pivot = p; }
void Model::setNormal(glm::vec3 n) { normal = n; }

