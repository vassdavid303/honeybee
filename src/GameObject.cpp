#include "GameObject.h"

GameObject::GameObject()
{
    modelsNum = 0;
}
void GameObject::addModel(Model& model)
{
    if(modelsNum < MAX_MODEL_NUM)
        models[modelsNum++]=&model;
    else
        printf("Too much models!");
}
void GameObject::render()
{
    for(int i=0; i<modelsNum; i++){
        //debug
        //printf("vertices num: %d\n", models[i]->getVerticesNum());

        models[i]->loadVBO();
        models[i]->render();
    }
}
unsigned int GameObject::getModelsNum() { return modelsNum; }
//movements
void GameObject::rotateY(float angle)
{
    for(int i=0; i<modelsNum; i++)
         models[i]->rotateY(angle);
}
void GameObject::move(float x, float y, float z)
{
    for(int i=0; i<modelsNum; i++)
         models[i]->move(x, y, z);
}
void GameObject::move(glm::vec3 v)
{
    for(int i=0; i<modelsNum; i++)
         models[i]->move(v);
}
void GameObject::rotate(float angle, glm::vec3 n)
{
    for(int i=0; i<modelsNum; i++)
         models[i]->rotate(angle, n);
}
void GameObject::mirror()
{

}
void GameObject::moveForward(float m)
{
    for(int i=0; i<modelsNum; i++)
         models[i]->moveForward(m);
}
void GameObject::scale(float s)
{
    for(int i=0; i<modelsNum; i++)
         models[i]->scale(s);
}
void GameObject::setPivot(glm::vec3 p)
{
    for(int i=0; i<modelsNum; i++)
        models[i]->setPivot(p);
}
void GameObject::setNormal(glm::vec3 n)
{
    for(int i=0; i<modelsNum; i++)
        models[i]->setNormal(n);
}
