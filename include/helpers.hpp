#ifndef HELPERS_HPP_INCLUDED
#define HELPERS_HPP_INCLUDED
//string functions
#include <string>
#include <string.h>
#include <sstream>
#include <fstream>


template<class T, size_t N>
constexpr size_t arrSize(T (&)[N]) {
    return N;
}
template<size_t N>
std::string mStrCat(std::string (&strArr)[N]){
    std::stringstream str;
    for(int i=0; i<N; i++){
        str << strArr[i];
    }
    return str.str();
}
bool fileexists(const char *filename);

#endif // HELPERS_HPP_INCLUDED
