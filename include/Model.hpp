#ifndef MODEL_H
#define MODEL_H


//string functions
#include <string>
#include <string.h>
#include <sstream>

//window handling
#include <windows.h>  // For MS Windows
#include <GL/glew.h>

#include <math.h>
#include <vector>
//
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/vec3.hpp> // glm::vec3
#include <glm/gtc/matrix_transform.hpp>

#include <fstream>

//my
#include "../inc/objloader.hpp"
#include "helpers.hpp"

//SOIL
#include "../inc/soil.h"


class Model
{
    public:
        Model(std::string uname);
        // getters:
        const unsigned int getId() { return id; }
        std::string getName();
        const std::vector<glm::vec2> getUvs();
        const std::vector<glm::vec3> getNormals();
        const std::vector<glm::vec3> getVertices();
        int getVertSize();
        int getVerticesNum();
        const glm::mat4x4 getTransform();
        const glm::vec3 getPivot();

        //transform
        void rotateY(float angle);
        void move(float x, float y, float z);
        void move(glm::vec3 v);
        void rotate(float angle, glm::vec3 n);
        void mirror();
        void moveForward(float m);
        void scale(float s);

        //buffer & render
        void render();
        void loadVBO();
        void loadTexture(std::string textName);

        // setters:
        void setPivot(glm::vec3 p);
        void setNormal(glm::vec3 n);

    protected:

    private:
        const unsigned int id;
        std::string name;
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec2> uvs;
        std::vector<glm::vec3> normals;
        glm::vec3 pivot;
        glm::vec3 normal;
        //buffers
        GLuint vertexbuffer;
        GLuint uvbuffer;
        GLuint normalbuffer;
        GLuint texture;
        glm::mat4x4 transform;
        GLint uniform_mytexture;

};

#endif // MODEL_H
