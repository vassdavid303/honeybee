#include "helpers.hpp"

bool fileexists(const char *filename) {
  std::ifstream ifile(filename);
  return (bool)ifile;
}
