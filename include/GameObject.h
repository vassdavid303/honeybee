#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#include "Model.hpp"
#define MAX_MODEL_NUM 10

class GameObject
{
    public:
        GameObject();
        void addModel(Model &model);
        void GameObject::createModel(std::string uname);
        void render();
        unsigned int getModelsNum();

        //movements
        void rotateY(float angle);
        void move(float x, float y, float z);
        void move(glm::vec3 v);
        void rotate(float angle, glm::vec3 n);
        void mirror();
        void moveForward(float m);
        void scale(float s);

        //setter
        void setPivot(glm::vec3 p);
        void setNormal(glm::vec3 n);

    protected:

    private:
        Model *models[MAX_MODEL_NUM];
        int z[10];
        unsigned int modelsNum;
        glm::vec3 position;
        glm::vec3 normal;

};

#endif // GAMEOBJECT_H
