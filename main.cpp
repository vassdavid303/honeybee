/** my libs **/
//



/** ext: **/
//shader+text



#include <stdio.h>
#include <stdlib.h>

//for test
#include <iostream>

//string functions
#include <string>
#include <string.h>
#include <sstream>

#include <windows.h>  // For MS Windows
#include "inc/ext/shader.h"
#include <GL/glew.h>


#include <math.h>
#include <vector>


#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>


//
#include "include/Model.hpp"
#include "include/GameObject.h"


#include "inc/shader.hpp"


//SOIL
#include "inc/soil.h"


/*
c++ 17 standard
*/

using namespace glm;


/*
 Represents a point light
 */
struct Light {
    glm::vec3 position;
    glm::vec3 intensities; //a.k.a. the color of the light
};


bool initGraph()
{
    if (!glfwInit()){
        printf("GLFW initialize failed");
        return -1;
    }



    // Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT_AND_BACK);
    return true;
}

bool createWindow(GLFWwindow* &window)
{
    window = glfwCreateWindow(1024, 768, "The Honey Bee", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return false;
    }
    glfwMakeContextCurrent(window);
}

bool initGLEW(){
     // Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}
}


class Game{
private:
    static unsigned int modelId;
public:
    Model models[10];
    void addModel(Model newModel){
        //models[Game::modelId]= newModel;
        //models[] = newModel;
        Game::modelId++;
    }
};

class Buffer{
private:
    GLuint vertexbuffer;
public:
    void createVBO(GLsizei bSize){
        // Generate 1 buffer, put the resulting identifier in vertexbuffer
        glGenBuffers(bSize, &vertexbuffer);
        // The following commands will talk about our 'vertexbuffer' buffer
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    }
    void makeBuffer(GLsizeiptr bufferSize, std::vector<glm::vec3> *vertices){
        glBufferData(GL_ARRAY_BUFFER, bufferSize, &vertices[0], GL_STREAM_DRAW);
    }
};


int main(void)
{


    GLFWwindow* window;
    /* init */
    initGraph();
    createWindow(window);
    initGLEW();

	/*/ Controls /*/
	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    // Hide the mouse and enable unlimited mouvement
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // Set the mouse at the center of the screen
    glfwPollEvents();
    glfwSetCursorPos(window, 1024/2, 768/2);


    // background color
	glClearColor(0.7f, 0.4f, 0.1f, 0.0f);


    int width, height;
    //get windows size
    glfwGetWindowSize(window, &width, &height);

    // Set the mouse at the center of the screen
    glfwPollEvents();
    glfwSetCursorPos(window, width/2, height/2);

    //models init

    //models
    Model bee("bee");
    Model ground("ground");
    Model kocka("kocka");
    Model margarete("margarete");
    Model wing("wing");


    /* Honey bee settings */
    GameObject honeyBee;

    //models
    Model leftWing("left_wing");
    Model rightWing("right_wing");
    Model eye("eye");
    Model jaw("jaw");
    Model legs("legs");
    Model body("body");
    Model petal("petal");
    Model stalk("flower_stalk");

    //test model
    // Model tiger("tiger");
    // tiger.loadTexture("tiger");
    // tiger.scale(.001);


    /* Model settings */
    //set pivot

    //body.rotate(30,glm::vec3(0,1,0));

    petal.loadTexture("petal");
    stalk.loadTexture("flower_stalk");



    //textures
    leftWing.loadTexture("wing");
    rightWing.loadTexture("wing");
    body.loadTexture("bee");
    eye.loadTexture("bee");
    jaw.loadTexture("bee");
    legs.loadTexture("bee");
    margarete.loadTexture("petal");

    //Player: add models
    honeyBee.addModel(eye);
    honeyBee.addModel(jaw);
    honeyBee.addModel(legs);
    honeyBee.addModel(leftWing);
    honeyBee.addModel(rightWing);
    honeyBee.addModel(body);
    //settings
    honeyBee.setPivot(glm::vec3(0.00014,5.07933,1.422213));
    honeyBee.setNormal(glm::vec3(0,0,1));
    //load textures
    wing.loadTexture("wing");
    ground.loadTexture("ground");
    kocka.loadTexture("ground");
    bee.loadTexture("bee");

   Shader basicMaterial("shaders/TransformVertexShader.vertexshader", "shaders/TextureFragmentShader.fragmentshader");

    // Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders( "shaders/TransformVertexShader.vertexshader", "shaders/TextureFragmentShader.fragmentshader" );
    // Get a handle for our "MVP" uniform
	GLuint matrixID = glGetUniformLocation(programID, "MVP");


	// Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	glm::mat4 projectionMat = glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.1f, 500.0f);
	// Camera matrix
	glm::mat4 viewMat  = glm::lookAt(
        glm::vec3(0,8,7), // Camera is at (4,3,3), in World Space
        glm::vec3(0,2,-5), // and looks at the origin
        glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
    );
	// Model matrix : an identity matrix (model will be at the origin)
	glm::mat4 modelMat      = glm::mat4(1.0f);
	// Our ModelViewProjection : multiplication of our 3 matrices
	glm::mat4 MVP        = projectionMat * viewMat * modelMat; // Remember, matrix multiplication is the other way around



    std::cout << "Start Loop";
    float mX=0, mY=0, mZ=0;

    glUseProgram(programID);
	GLuint lightID = glGetUniformLocation(programID, "LightPosition_worldspace");
	glm::vec3 lightPos = glm::vec3(0,30,0);

    while ( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS && glfwWindowShouldClose(window) == 0 )
    {

        static double lastTime = glfwGetTime();

        // Compute time difference between current and last frame
        double currentTime = glfwGetTime();
        float deltaTime = float(currentTime - lastTime);
        // Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		 // shader + texture //
		// _________________//
		// Use our shader
		//glUseProgram(programID);
		//lighting
		glUniform3f(lightID, lightPos.x, lightPos.y, lightPos.z);

		// Send our transformation to the currently bound shader,
		// in the "MVP" uniform
		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &MVP[0][0]);

        glColor3d(.1,.7,.3);


        /*/ Controls /*/
        mZ=mX=mY=0;

        // Move forward
        if (glfwGetKey( window, GLFW_KEY_W ) == GLFW_PRESS){
            mZ=-.02*deltaTime;
        }
        // Move backward
        if (glfwGetKey( window, GLFW_KEY_S ) == GLFW_PRESS){
            mZ=.02*deltaTime;
        }
        // Strafe right
        if (glfwGetKey( window, GLFW_KEY_D ) == GLFW_PRESS){
            mX=-.002*deltaTime;
        }
        // Strafe left
        if (glfwGetKey( window, GLFW_KEY_A ) == GLFW_PRESS){
            mX=.002*deltaTime;
        }
        // Up
        if (glfwGetKey( window, GLFW_KEY_R ) == GLFW_PRESS){
            mY=.005*deltaTime;
        }
        // Strafe left
        if (glfwGetKey( window, GLFW_KEY_F) == GLFW_PRESS){
            mY=-.005*deltaTime;
        }

        /*/ Rendering object /*/
        ground.loadVBO();
        ground.render();
        petal.loadVBO();
        petal.render();
        //kocka.loadVBO();
        //kocka.render();
        honeyBee.render();
        //test obj
        //tiger.loadVBO();
        //tiger.render();

        //bee.loadVBO();
        //bee.render();

        if(mZ!=0)
            honeyBee.moveForward(mZ);
        if(mX!=0)
            honeyBee.rotate(mX, glm::vec3(0,1,0));//bee.rotateY(mX);//bee.rotate(mX,glm::vec3(0,1,0));
        if(mY!=0)
            honeyBee.move(0, mY, 0);

        //get transform matrix
        modelMat = body.getTransform();

        /*/ Camera handling /*/
        MVP  = projectionMat * viewMat * modelMat;
        /* Swap front and back buffers */
        glfwSwapBuffers(window);
        /* Poll for and process events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
